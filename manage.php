<?php
if ($_POST['esr_save'] && $_POST['esr_save'] != "") {

    extract($_POST);
    $deprecated = null;
    $autoload = 'no';

    if (get_option('esr_template_email_from') !== false) {
        update_option('esr_template_email_from', $email_from);
    } else {
        add_option('esr_template_email_from', $email_from, $deprecated, $autoload);
    }

    if (get_option('esr_template_title') !== false) {
        update_option('esr_template_title', $title);
    } else {
        add_option('esr_template_title', $title, $deprecated, $autoload);
    }

    if (get_option('esr_template_description') !== false) {
        update_option('esr_template_description', $description);
    } else {
        add_option('esr_template_description', $description, $deprecated, $autoload);
    }

    if (get_option('esr_template_footer') !== false) {
        update_option('esr_template_footer', $footer);
    } else {
        add_option('esr_template_footer', $footer, $deprecated, $autoload);
    }
    
    if (get_option('esr_if_form_popup') !== false) {
        update_option('esr_if_form_popup', $esrpopup);
    } else {
        add_option('esr_if_form_popup', $esrpopup, $deprecated, $autoload);
    }
}
$emailFromVal = $_POST['email_from'] ? $_POST['email_from'] : get_option('esr_template_email_from');
$titleVal = $_POST['title'] ? $_POST['title'] : get_option('esr_template_title');
$descriptionVal = $_POST['description'] ? $_POST['description'] : get_option('esr_template_description');
$footerVal = $_POST['footer'] ? $_POST['footer'] : get_option('esr_template_footer');

$esrPopup = $_POST['esrpopup'] ? $_POST['esrpopup'] : get_option('esr_if_form_popup');

$esrIfPopupChecked = $esrPopup ? 'checked' : "";

?>
<style type="text/css">
    .esr_settings label{
        width: 20%;
        display: inline-block;
        font-weight: bold;
        text-align: right;
        padding-right: 10px;
        padding-bottom: 10px;
    }
    .esr_settings input[type='text'],
    .esr_settings textarea{
        width: 40%;
    }
    .esr_settings h3{
        margin-left: 20%;
        padding-left: 10px;
    }

</style>
<div class="esr_settings">
    <h1>Global Settings For Email Search Results</h1>
    <hr/>
    <div class="">
        <h3>Email Template Settings</h3>
        <form method="post" action="#">
            <p>
                <label for="emaile-from">Email From:  </label>
                <input type="text" name="email_from" id="email-from" value="<?php echo $emailFromVal; ?>" />
            </p>
            <p>
                <label for="title">Email Title:  </label>
                <input type="text" name="title" id="title" value="<?php echo $titleVal; ?>" />
            </p>
            <p>
                <label for="description">Email Description:</label>
                <textarea id="description" rows="3" name="description"><?php echo $descriptionVal; ?></textarea>
            </p>
            <p>
                <label for="footer">Email Footer:</label>
                <textarea id="footer" rows="3" name="footer"><?php echo $footerVal; ?> </textarea>
            </p>
            <p>
                <label for="esrpopup">Show Email Form as a popup:</label>
                <input type="checkbox" id="esrpopup" name="esrpopup" <?php echo $esrIfPopupChecked; ?> > 
            </p>
            <p>
                <label></label>
                <input type="submit" value="Save Settings" name="esr_save"/>
            </p>
        </form>
    </div>
    <hr/>
</div>
