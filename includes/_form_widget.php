<?php

// BEGIN NEWS/Lrahos Widget 
add_action( 'widgets_init', 'register_esr_widget' );

function register_esr_widget() {
    register_widget( 'Esr_Widget' );
}

class Esr_Widget extends WP_Widget {
    
    function Esr_Widget() {
        $widget_ops = array( 'classname' => 'esr_widget', 'description' => __('Displays Email Search Result Form on search page', 'kino') );
        $control_ops = array( 'id_base' => 'esr-widget' );
        $this->WP_Widget( 'esr-widget', __('Esr :: Email Search Result', 'kino'), $widget_ops, $control_ops );
    }
    function widget( $args, $instance ){
        
        if ( is_search() ){
            
            if ( ! empty( $instance['title'] ) ) {
                echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
            }
            
            require_once 'class-email-search.php';
            
            $search = new Email_Search_Result();
            echo $search->widgetFormContent();
            
        }
        
    ?>    
       
       
       
    <?php
    }
    
    /**
    * Back-end widget form.
    *
    * @see WP_Widget::form()
    *
    * @param array $instance Previously saved values from database.
    */
   public function form( $instance ) {
       
        $title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'Email Search Result', 'wpsight' );
        ?>
        <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <?php 
   }
   
   /**
    * Sanitize widget form values as they are saved.
    *
    * @see WP_Widget::update()
    *
    * @param array $new_instance Values just sent to be saved.
    * @param array $old_instance Previously saved values from database.
    *
    * @return array Updated safe values to be saved.
    */
   public function update( $new_instance, $old_instance ) {
           $instance = array();
           $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

           return $instance;
   }
}
// END NEWS/Lrahos Widget 
