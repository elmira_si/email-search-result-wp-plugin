<?php

// BEGIN NEWS/Lrahos Widget 
add_action( 'widgets_init', 'register_esr_widget' );

function register_esr_widget() {
    register_widget( 'Esr_Widget' );
}

class Esr_Widget extends WP_Widget {
    function Esr_Widget() {
        $widget_ops = array( 'classname' => 'esr_widget', 'description' => __('Displays Email Search Result Form on search page', 'kino') );
        $control_ops = array( 'id_base' => 'esr-widget' );
        $this->WP_Widget( 'esr-widget', __('Esr :: Email Search Result', 'kino'), $widget_ops, $control_ops );
    }
    function widget( $args, $instance ){
        
        if ( is_search() ){
            global $wpdb;
            $emailLists = $wpdb->get_results("SELECT * FROM `email_search_result_list` ");
            
            
            
            $dir = plugin_dir_path( __FILE__ );
            
            if ( isset( $_POST['email_addresses'] ) ){
                
                $emailTo = $_POST['email_addresses'];
                
                $emailListiD = (int) $_POST['email_list'];
             
                if ( $emailListiD != "" ){
                    $emails = $wpdb->get_row("SELECT * FROM `email_search_result_list` WHERE id = " . $emailListiD );
                    $allAddressess = $emails->addresses;
                    $multiple_recipients = explode(";", $allAddressess);
                } else {
                    $multiple_recipients = $emailTo;
                }
                //var_dump($multiple_recipients);die;
                
                
                $emailFrom = get_option('esr_template_email_from');
                $tpl_title = get_option('esr_template_title');
                $tpl_description = get_option('esr_template_description');
                $tpl_footer = get_option('esr_template_footer');
                
                $subject = $_POST['email_subject'] ? $_POST['email_subject'] : 'Spaincoastrealty Best Offers For You';
                
                //Prepare headers for HTML
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'From: Spaincoastrealty Best Offer <'.$emailFrom.'>' . "\r\n";
                
                $searchContent = $this->search_result_content();
                
                //Get e-mail template     
                $message_template = file_get_contents($dir . 'email-templates/clasic-light.html'); 
                //var_dump($message_template);
                
                $message = str_ireplace('[search-post-loop-content]', $searchContent , $message_template); 
                $message = str_ireplace('[email-tpl-title]', $tpl_title, $message); 
                $message = str_ireplace('[email-tpl-description]', $tpl_description, $message); 
                $message = str_ireplace('[email-tpl-footer]', $tpl_footer, $message); 
                
               
                wp_mail($multiple_recipients, $subject, $message, $headers);
                
            }
            
            echo "<div id='form_widger'>";
            echo "<h4 class='esr_title'>Email This Search Result </h4>";
            echo "<form method='post'>";
            echo "<input type='text' name='email_subject' placeholder='Subject: Coastrealty Offers' required >";            
            if ( is_user_logged_in() ) { 
                echo "<input type='text' name='email_addresses' placeholder='To: example@gmail.com' >";
                 echo "<select name='email_list'>";
                    echo "<option> --- Select Email List --- </option>";
                    if( !empty( $emailLists ) ){
                        foreach ($emailLists as $list ) {
                            echo "<option value='".$list->id."'>".$list->title."</option>";
                        }
                    }
                 echo "</select>";
                 
            }else{
                echo "<input type='text' name='email_addresses' placeholder='To: example@gmail.com' required >";
            }
            echo "<input type='submit' name='email_result' value='Send Result By Email' class='moretag btn btn-primary' >";
            echo "</form>";
            
            echo "</div>";
            
        }
        
    ?>    
       
       
       
    <?php
    }
}
// END NEWS/Lrahos Widget 
