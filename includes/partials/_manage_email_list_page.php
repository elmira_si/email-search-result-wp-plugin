
<div class="list_row">
<?php if( isset($_GET['edit_email_list']) && $_GET['edit_email_list'] != "" ){ ?>
    <a href="<?php echo $actionUrl; ?>">Add New Email List</a>
    <h1>Update Email Lists</h1>
    <div class="list_left">   
        <form action="<?php echo $actionUrl; ?>" method="post" >
            <div>
                <label for="list-title" >List Title</label>
                <input id="list-title" type="text" name="title" value="<?php echo $updateRow->title; ?>"/>
                <input id="list-title" type="hidden" name="id" value="<?php echo $updateRow->id; ?>"/>
            </div>
            <div>
                <label for="list-emails">Email Addresses</label>
                <textarea id="list-emails" name="addresses" placeholder="example@google.com; info@example.com; test@mail.am"><?php echo $updateRow->addresses; ?></textarea>
                <div class="msg" >All emails must be seperated by simbole 
                    <span>;</span> <br/>
                    <span style="font-size:10px; margin-top: 5px;">We dont need last seperator ;</span>
                </div>
            </div>            
            <input id="save-list" name="update_email_list" type="submit" value="Update Email List" />
        </form>
    </div>

<?php } else { ?>
    <h1>Add Email Lists</h1>
    <div class="list_left">   
        <form action="#" method="post" >
            <div>
                <label for="list-title" >List Title</label>
                <input id="list-title" type="text" name="title" />
            </div>
            <div>
                <label for="list-emails">Email Addresses</label>
                <textarea id="list-emails" name="addresses" placeholder="example@google.com; info@example.com; test@mail.am"></textarea>
                <div class="msg" >All emails must be seperated by simbole 
                    <span>;</span> 
                    <br/>
                    <span style="font-size:10px; margin-top: 5px;">We don't need last seperator ;</span>
                </div>
            </div>            
            <input id="save-list" name="add_email_list" type="submit" value="Add New List" />
        </form>
    </div>

<?php } ?>
    
    
    <div class="list_right">
        <div class="list_title">Email Lists</div>
        <?php 
            if( !empty($getAllList) ){             
                foreach ($getAllList as $value) {
        ?>
                <div class="list_right_row">
                    <label><?php echo $value->title; ?></label>
                    <a href="<?php echo $actionUrl; ?>&edit_email_list=<?php echo $value->id; ?> ">Edit</a> | 
                    <a href="<?php echo $actionUrl; ?>&del_email_list=<?php echo $value->id; ?> " >Delete</a>
                </div>
            <?php } ?>
        <?php } ?>        
    </div>
</div>


<style type="text/css">
    .list_row{
        overflow: auto;
        font-size: 13px;
        padding: 20px;
    }
    .list_row .list_left{
        width: 30%;
        float: left;
        margin-right: 50px;
    }
    .list_row .list_left label{
        font-weight: bold;
        display: block;
        width: 100%;
        box-sizing: border-box;
    }
    .list_row .list_left input#list-title,
    .list_row .list_left textarea{
        width: 100%;
    }
    .list_row .list_left input#list-title{        
        margin-bottom: 10px;
    }
    .list_row .list_left .msg{
        font-size: 10px;
        color:#999;
        font-style:oblique;
    }
    .list_row .list_left .msg span{
        font-size:15px;
        font-weight:bold;
        color: #000;
        font-style: normal;
        font-family: 'arial';
        line-height: 10px;
        background-color: #FFBA00;
        padding: 1px;
    }
    .list_row .list_left #save-list{
        margin-top: 20px;
        padding: 5px 20px;
        background-color: #0074A2;
        border: none;
        color: #fff;
        font-size: 18px;
        cursor: pointer;
    }
    .list_row .list_left #save-list:hover{
        background-color: #0491CA;
    }
    .list_row .list_right{
        width: 40%;
        float: left;
        border: 1px solid #002a80;
        padding: 5px 15px;
        text-align: right;
    }
    
    .list_right_row{
        overflow: auto;
        margin-bottom: 10px;
        border-bottom: 1px dotted #0074A2;
        padding-bottom: 10px;
    }
    .list_right .list_right_row:last-child{
        border-bottom: 0;
    }
    .list_row .list_right .list_title{
        font-weight: bold;
        font-size: 18px;
        margin-bottom: 15px;
        border-bottom: 1px solid #0074A2;
        text-align: left; 
        padding: 10px 0;
        color: #0074A2;
    }
    .list_row .list_right a{
        color: #0074A2;
    }
    .list_row .list_right a:hover{
        color: #f00;
    }
    .list_row .list_right label{        
        width: 80%;
        float: left;
        text-align: left;
        cursor: auto;
        font-weight: bold;
    }
    
</style>