<?php
/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package     Email Search Result
 * @subpackage  email-search-result/includes
 */

/**
 * The core plugin class.
 * 
 * @package     Email Search Result
 * @subpackage  email-search-result/includes
 * @author      Elmira Kirakosyan <elmirakirakosyan@gmail.com>
 */
class Email_Admin_Settings {
    
    private $table_name;
    private $db;
    public $errors = array();
	
    public function __construct() { 
        
        global $wpdb;
        $this->table_name = $table_name;
        $this->db = $wpdb;
        
        require_once '_form_widget.php';
        
        add_action( 'admin_menu', array( $this, 'email_search_result_settings' ) );  
        add_action( 'admin_menu', array( $this, 'email_search_list_settings'));
    }
    function email_search_result_settings () {
        add_menu_page( 'Email Search Result Settings','Email Search Result','manage_options','email-search-result/manage.php', '', plugins_url( 'email-search-result/images/menuIcon.png' ) );
    }
    
    function email_search_list_settings() {
        add_submenu_page('email-search-result/manage.php', 'Manage Email Lists', 'Manage Email Lists', 'manage_options', 'email-search-list', array($this,'email_search_list_settings_callback') );
	
    }
    
    public function setTable($table_name) {
        $this->table_name = $table_name;
    }
    public function getRow($id) {
        return $this->db->get_row("SELECT * FROM " . $this->table_name . " WHERE id= " . (int) $id);
    }  
     public function findAll() {
        return $this->db->get_results("SELECT * FROM " . $this->table_name);
    }    
    public function updateRow($data, $where) {        
       return $this->db->update($this->table_name, $data, $where );
    }
    public function deleteRow( $where ){
        $this->db->delete($this->table_name, $where);
    }

    function email_search_list_settings_callback() {

        $this->setTable('email_search_result_list');
        
        $actionUrl = get_admin_url(). 'admin.php?page=email-search-list';
        $getAllList = $this->findAll();

        if( $_POST['add_email_list'] ){
            $saveData = $_POST;
            unset( $saveData['add_email_list'] );

            $save = $this->db->insert($this->table_name, $saveData , array('%s', '%s'));
            if( $save ){
                $message = "New Email List Saved!";
                wp_redirect( $actionUrl ); exit;
            } 
        }
        if( $_POST['update_email_list'] ){
            
            $updateId = $_POST['id'];
            $updateData = $_POST;
            unset( $updateData['update_email_list'] );
            
            //var_dump($updateData);
            $where = array('id' => (int) $updateId);
            $update = $this->updateRow( $updateData, $where );
            if( $update ){
                $message = "Email List Updated!";
                wp_redirect( $actionUrl ); exit;
            } 
        }
        //get row for update data
        if( isset($_GET['edit_email_list']) && $_GET['edit_email_list'] != "" ){
            $updateRow = $this->getRow( $_GET['edit_email_list'] );
        }
        //Delete Lists functions
        if( isset($_GET['del_email_list']) && $_GET['del_email_list'] != "" ){
            $where = array('id' => (int) $_GET['del_email_list'] );
            
            $this->deleteRow( $where );
            $message = "Email List Deleted.";
            wp_redirect( $actionUrl ); exit; 
            
        }

        require_once dirname(__FILE__) . '/partials/_manage_email_list_page.php';

    }
    
    
   
    
}
