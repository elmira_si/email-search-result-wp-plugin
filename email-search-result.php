<?php
/**
 * Email Search Result
 *
 * @wordpress-plugin
 * Plugin Name:       Email Search Result
 * Plugin URI:        https://bitbucket.org/elmira_k82/email-search-result-wp-plugin/src
 * Description:       Plugin allows to send serch results by mail or selected mail list
 * Version:           1.0.0
 * Author:            Elmira K.
 * Author URI:        http://testings.info
 * Text Domain:       email-search-result 
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

register_activation_hook(__FILE__, 'ls_install');

function ls_install() {

    global $wpdb;
   
    $emailLists = "CREATE TABLE IF NOT EXISTS `email_search_result_list` (
        `id` int(15) unsigned NOT NULL AUTO_INCREMENT,
        `title` varchar(100) COLLATE utf8_bin DEFAULT NULL,
        `addresses` text COLLATE utf8_bin,        
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

    dbDelta($emailLists);
}

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-email-search.php';
require plugin_dir_path( __FILE__ ) . 'includes/class-admin-settings.php';

//allow redirection, even if my theme starts to send output to the browser
add_action('init', 'do_output_buffer');
function do_output_buffer() {
        ob_start();
}

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_email_search_result() {
    $plugin = new Email_Search_Result();
    $plugin = new Email_Admin_Settings();
	
}
run_email_search_result();