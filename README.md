# README #

# WP plugin - Email Search Result #

## Plugin allows to send serch result by mail ##

![2015-07-26_0736.png](https://bitbucket.org/repo/aXMK5R/images/641078408-2015-07-26_0736.png)

**You can use plugin as a widget or as a popup fixed buttom, all settings are editable from wp-admin**

![2015-08-01_2216.png](https://bitbucket.org/repo/aXMK5R/images/2657744344-2015-08-01_2216.png)

![2015-09-10_1135.png](https://bitbucket.org/repo/aXMK5R/images/4234508552-2015-09-10_1135.png)

![2015-09-10_1132.png](https://bitbucket.org/repo/aXMK5R/images/206411855-2015-09-10_1132.png)

**Email settings and mail conten ediable from wp-admin**

![2015-09-10_1132.png](https://bitbucket.org/repo/aXMK5R/images/821395007-2015-09-10_1132.png)

![2015-08-19_1414.png](https://bitbucket.org/repo/aXMK5R/images/584190301-2015-08-19_1414.png)


you can find all screenshots in the source folder...

